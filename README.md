# Dotfiles

Dotfiles for my dev/research environment.

So far they contain:
- Neovim dev environment.
- A Tmux config with tpm, and sensible defaults.
- Alacritty config with lowered opacity and custom fonts.
- Custom fish command prompt.
- Fish config with any other random aliases and functions.

See the [snippet](https://gitlab.com/disastrpc/dotfiles/-/snippets/3675341) for instructions on cloning.

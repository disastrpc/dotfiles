-- Load local user configs.
-- The plugins/init.lua file will in turn load all plugin configurations
-- inside plugins/*.lua.
local config_mods = {'opts', 'plug', 'vars', 'plugins', 'keys', 'theme'}
for i = 1, 6 do require(config_mods[i]) end

-- Loads all plugins inside the plugins directory
local blacklisted = { '../', './', 'init.lua' }
local plugin_src = 'plugins.'

-- Check if file is blacklisted
function contains(table, value)
  for i = 1,#blacklisted do
    if (blacklisted[i] == value) then
      return true
    end
  end
  return false
end

-- Loop through and require plugins
for _, file in ipairs(vim.fn.readdir(vim.fn.stdpath('config')..'/lua/' .. plugin_src:gsub('%.',''), [[v:val =~ '\.lua$']])) do
  if contains(blacklisted, file) then goto continue end
  require(plugin_src .. file:gsub('%.lua$', ''))
  ::continue::
end

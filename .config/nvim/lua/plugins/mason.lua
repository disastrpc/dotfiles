-- Setup Mason and LSP
local navic = require('nvim-navic')
require('mason').setup()
require('mason-lspconfig').setup()

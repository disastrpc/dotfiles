-- Personal
vim.keymap.set('n', 'J', 'mzJ`z')
vim.keymap.set('n', '<C-d>', '<C-d>zz')
vim.keymap.set('n', '<C-u>', '<C-u>zz')
vim.keymap.set('n', 'n', 'nzzzv')
vim.keymap.set('n', 'N', 'Nzzzv')

vim.keymap.set('x', '<leader>p', [['_dP]])

vim.keymap.set({'n', 'v'}, '<leader>y', [["+y]])
vim.keymap.set('n', '<leader>Y', [['+Y]])

vim.keymap.set('n', '<Enter>', 'o<Esc>k')
vim.keymap.set('n', '<S-Enter>', 'O<Esc>j')

vim.keymap.set('n', '<C-k>', '<cmd>cnext<CR>zz')
vim.keymap.set('n', '<C-j>', '<cmd>cprev<CR>zz')
vim.keymap.set('n', '<C-K>', '<cmd>lnext<CR>zz')
vim.keymap.set('n', '<C-J>', '<cmd>lprev<CR>zz')

vim.keymap.set('n', '<A-K>', '<cmd>horizontal res -1<CR>', { desc = '-1 hor res' })
vim.keymap.set('n', '<A-J>', '<cmd>horizontal res +1<CR>', { desc = '+1 hor res' })
vim.keymap.set('n', '<A-H>', '<cmd>vertical res -1<CR>', { desc = '-1 ver res' })
vim.keymap.set('n', '<A-L>', '<cmd>vertical res +1<CR>', { desc = '+1 ver res' })

vim.keymap.set('n', '<leader>bn', '<cmd>bnext<CR>', { desc = 'Next buffer'})
vim.keymap.set('n', '<leader>bp', '<cmd>bprevious<CR>', { desc = 'Prev buffer'})
vim.keymap.set('n', '<leader>bc', '<cmd>bdelete<CR>', { desc = 'Close buffer'})

-- Disable arrow keys cause I hate myself
vim.keymap.set('n', '<Up>', '<Nop>', { noremap = true, silent = true})
vim.keymap.set('n', '<Down>', '<Nop>', { noremap = true, silent = true})
vim.keymap.set('n', '<Left>', '<Nop>', { noremap = true, silent = true})
vim.keymap.set('n', '<Right>', '<Nop>', { noremap = true, silent = true})
vim.keymap.set('i', '<Up>', '<Nop>', { noremap = true, silent = true})
vim.keymap.set('i', '<Down>', '<Nop>', { noremap = true, silent = true})
vim.keymap.set('i', '<Left>', '<Nop>', { noremap = true, silent = true})
vim.keymap.set('i', '<Right>', '<Nop>', { noremap = true, silent = true})

vim.keymap.set('n', '<leader>s', [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]], { desc = 'Replace current word' })
vim.keymap.set('n', '<leader>X', '<cmd>!chmod +x %<CR>', { silent = true, desc = 'Make executable' })

vim.keymap.set('n', '<leader>tsc', '<cmd>TSContextToggle<CR>', { desc = 'TreeSitter: Toggle context' })
vim.keymap.set('n', '<leader>E', '<cmd>Vexplore!<CR>', { desc = 'Open file explorer' })

-- Undotree
vim.keymap.set('n', '<leader>U', vim.cmd.UndotreeToggle, { desc = 'Undotree: Toggle' })

-- Vimspector
vim.cmd([[
nmap <F9> <cmd>call vimspector#Launch()<cr>
nmap <F5> <cmd>call vimspector#StepOver()<cr>
nmap <F8> <cmd>call vimspector#Reset()<cr>
nmap <F11> <cmd>call vimspector#StepOver()<cr>')
nmap <F12> <cmd>call vimspector#StepOut()<cr>')
nmap <F10> <cmd>call vimspector#StepInto()<cr>')
]])

vim.keymap.set('n', 'Db', ':call vimspector#ToggleBreakpoint()<cr>')
vim.keymap.set('n', 'Dw', ':call vimspector#AddWatch()<cr>')
vim.keymap.set('n', 'De', ':call vimspector#Evaluate()<cr>')

-- Setup keybindings
local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>ff', builtin.find_files, { desc = 'Find files' })
vim.keymap.set('n', '<leader>fg', builtin.live_grep, { desc = 'Live grep' })
vim.keymap.set('n', '<leader>fb', builtin.buffers, { desc = 'Search buffers' })
vim.keymap.set('n', '<leader>fh', builtin.help_tags, { desc = 'Search tags' })

vim.keymap.set('n', '<leader>xx', function() require('trouble').open() end, { desc = 'Trouble' })
vim.keymap.set('n', '<leader>xw', function() require('trouble').open('workspace_diagnostics') end, { desc = 'Workspace Diag'})
vim.keymap.set('n', '<leader>xd', function() require('trouble').open('document_diagnostics') end, { desc = 'Document Diag'})
vim.keymap.set('n', '<leader>xq', function() require('trouble').open('quickfix') end, { desc = 'Quickfix' })
vim.keymap.set('n', '<leader>xl', function() require('trouble').open('loclist') end, { desc = 'Localizations' })
vim.keymap.set('n', 'gR', function() require('trouble').open('lsp_references') end)

vim.keymap.set('n', '<leader>ft', ':FloatermNew --name=myfloat --height=0.8 --width=0.7 --autoclose=2 fish <CR> ')
vim.keymap.set('n', 't', ':FloatermToggle myfloat<CR>')
vim.keymap.set('t', '<Esc>', '<C-\\><C-n>:q<CR>')

vim.keymap.set('n', ']t', function()
  require('todo-comments').jump_next()
end, { desc = 'Next todo comment' })

vim.keymap.set('n', '[t', function()
  require('todo-comments').jump_prev()
end, { desc = 'Previous todo comment' })

vim.keymap.set({ 'n' }, '<Leader>k', function()
	require('lsp_signature').toggle_float_win()
end, { silent = true, noremap = true, desc = 'Toggle symbol signature' })

-- Glow
vim.keymap.set('n', '<leader>mp', '<cmd>Glow<CR>')

-- Aerial mapping
vim.keymap.set('n', '<leader>a', '<cmd>AerialToggle!<CR>', {desc = 'Toggle symbols'})

-- Gitsigns
vim.keymap.set('n', '<leader>hb', '<cmd>Gitsigns toggle_current_line_blame<CR>', { desc = 'Git: Toggle line blame' })
vim.keymap.set('n', '<leader>hh', '<cmd>Gitsigns toggle_linehl<CR>', { desc = 'Git: Toggle line highlight' })
vim.keymap.set('n', '<leader>hN', '<cmd>Gitsigns toggle_numhl<CR>', { desc = 'Git: Toggle num highlight' })
vim.keymap.set('n', '<leader>hx', '<cmd>Gitsigns toggle_deleted<CR>', { desc = 'Git: Toggle deleted lines' })
vim.keymap.set('n', '<leader>hv', '<cmd>Gitsigns toggle_signs<CR>', { desc = 'Git: Toggle signs' })
vim.keymap.set('n', '<leader>hw', '<cmd>Gitsigns toggle_word_diff<CR>', { desc = 'Git: Toggle word diff' })
-- Refactor mappings
vim.keymap.set('x', '<leader>re', ':Refactor extract ', { desc = 'Refactor: Extract' })
vim.keymap.set('x', '<leader>rf', ':Refactor extract_to_file ', { desc = 'Refactor: Extract to file' })
vim.keymap.set('x', '<leader>rv', ':Refactor extract_var ', { desc = 'Refactor: Extract var' })
vim.keymap.set({ 'n', 'x' }, '<leader>ri', ':Refactor inline_var', { desc = 'Refactor: Inline var' })
vim.keymap.set( 'n', '<leader>rI', ':Refactor inline_func', { desc = 'Refactor: Inline func' })
vim.keymap.set('n', '<leader>rb', ':Refactor extract_block', { desc = 'Refactor: Extract block' })
vim.keymap.set('n', '<leader>rbf', ':Refactor extract_block_to_file', { desc = 'Refactor: Extract block to file' })

vim.keymap.set('n', '<space>d', vim.diagnostic.open_float)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist)

vim.api.nvim_create_autocmd('LspAttach', {
  group = vim.api.nvim_create_augroup('UserLspConfig', {}),
  callback = function(ev)
    -- Enable completion triggered by <c-x><c-o>
    vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

    local opts = { buffer = ev.buf }
    vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
    vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
    vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
    vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
    vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)
    vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, opts)
    vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, opts)
    vim.keymap.set('n', '<space>wl', function()
      print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end, opts)
    vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, opts)
    vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, opts)
    vim.keymap.set({ 'n', 'v' }, '<space>ca', vim.lsp.buf.code_action, opts)
    vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
    vim.keymap.set('n', '<space>f', function()
      vim.lsp.buf.format { async = true }
    end, opts)
  end,
})


-- Harpoon
local harpoon = require('harpoon')

vim.keymap.set("n", "<leader>A", function() harpoon:list():add() end)
vim.keymap.set("n", "<leader>e", function() harpoon.ui:toggle_quick_menu(harpoon:list()) end, { desc = "Harpoon: Open window" })

vim.keymap.set("n", "<C-h>", function() harpoon:list():select(1) end)
vim.keymap.set("n", "<C-t>", function() harpoon:list():select(2) end)
vim.keymap.set("n", "<C-n>", function() harpoon:list():select(3) end)
vim.keymap.set("n", "<C-s>", function() harpoon:list():select(4) end)

-- Toggle previous & next buffers stored within Harpoon list
vim.keymap.set("n", "<leader>P", function() harpoon:list():prev() end, { desc = "Harpoon: Prev buffer"})
vim.keymap.set("n", "<leader>N", function() harpoon:list():next() end, { desc = "Harpoon: Next buffer" })

-- Autocommands
vim.cmd([[
set signcolumn=yes
autocmd CursorHold * lua vim.diagnostic.open_float(nil, { focusable = false })
]])

vim.api.nvim_create_autocmd({ 'BufWritePost' }, {
	callback = function()
		require('lint').try_lint()
	end,
})

vim.cmd([[
set signcolumn=yes
autocmd CursorHold * lua vim.diagnostic.open_float(nil, { focusable = false })
]])
-- Vimspector options
vim.cmd([[
let g:vimspector_sidebar_width = 85
let g:vimspector_bottombar_height = 15
let g:vimspector_terminal_maxwidth = 70
]])

-- Harpoon menu transparency
vim.api.nvim_create_autocmd({ 'FileType' }, {
	pattern = 'harpoon',
	callback = function()
		vim.opt.winblend = 20
	end,
})

-- Start lazy package manager
local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable',
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

local plugins = {
	{ 'nvim-lua/plenary.nvim' },
	{ 'williamboman/mason.nvim' },
	{ 'williamboman/mason-lspconfig.nvim' },
	{ 'neovim/nvim-lspconfig' },
	{ 'nvim-tree/nvim-web-devicons' },
	{ 'nvim-treesitter/nvim-treesitter' },
	{ 'nvim-treesitter/nvim-treesitter-context' },
	{ 'mfussenegger/nvim-lint' },
	{ 'arkav/lualine-lsp-progress' },
	{ 'nvim-lualine/lualine.nvim' },
	{ 'simrat39/rust-tools.nvim' },
	{ 'tpope/vim-fugitive' },
	{ 'hrsh7th/cmp-buffer' },
	{ 'hrsh7th/cmp-nvim-lsp' },
	{ 'hrsh7th/cmp-nvim-lsp-signature-help' },
	{ 'hrsh7th/cmp-nvim-lua' },
	{ 'hrsh7th/cmp-path' },
	{ 'hrsh7th/cmp-vsnip' },
	{ 'hrsh7th/nvim-cmp' },
	{ 'hrsh7th/vim-vsnip' },
	{ 'hrsh7th/vim-vsnip-integ' },
	{ 'puremourning/vimspector' },
	{ 'tpope/vim-surround' },
	{ 'RRethy/vim-illuminate' },
	{ 'voldikss/vim-floaterm' },
	{ 'm-demare/hlargs.nvim' },
	{ 'lewis6991/gitsigns.nvim' },
	{ 'rafamadriz/friendly-snippets' },
	{ 'navarasu/onedark.nvim' },
	{ 'mbbill/undotree' },
	{
		"ThePrimeagen/harpoon",
		branch = "harpoon2",
		dependencies = { "nvim-lua/plenary.nvim" }
	},
	{
		"pmizio/typescript-tools.nvim",
		dependencies = { "nvim-lua/plenary.nvim", "neovim/nvim-lspconfig" },
		opts = {},
	},
	{
		'ggandor/leap.nvim',
		lazy = false,
		dependencies = { 'tpope/vim-repeat' }
	},
	{ 
		'echasnovski/mini.clue',
		version = '*' 
	},
	{
		'echasnovski/mini.move',
		version = '*'
	},
	{ 'lukas-reineke/indent-blankline.nvim', main = 'ibl', opts = {} },
	{
		'nvim-telescope/telescope.nvim', tag = '0.1.3',
		dependencies = { 'nvim-lua/plenary.nvim' }
	},
	{
		'folke/todo-comments.nvim',
		dependencies = { 'nvim-lua/plenary.nvim' },
		opts = {}
	},
	{
		'folke/trouble.nvim',
		dependencies = { 'nvim-tree/nvim-web-devicons' },
		opts = {}
	},
	{
		'windwp/nvim-autopairs',
		event = 'InsertEnter',
		opts = {}
	},
	{
		'numToStr/Comment.nvim',
		lazy = false,
		opts = {},
	},
	-- Disabled to try harpoon
	-- {
	-- 	'romgrk/barbar.nvim',
	-- 	dependencies = {
	-- 		'lewis6991/gitsigns.nvim',
	-- 		'nvim-tree/nvim-web-devicons',
	-- 	},
	-- 	init = function() vim.g.barbar_auto_setup = false end,
	-- 	opts = {
	-- 	},
	-- 	version = '^1.0.0',
	-- },
	{
		'utilyre/barbecue.nvim',
		name = 'barbecue',
		version = '*',
		dependencies = {
			'SmiteshP/nvim-navic',
			'nvim-tree/nvim-web-devicons', -- optional dependency
		},
		opts = {
			-- configurations go here
		},
	},
	{
		'ray-x/lsp_signature.nvim',
		event = 'VeryLazy',
		opts = {},
		config = function(_, opts) require'lsp_signature'.setup(opts) end
	},
	{
		'stevearc/aerial.nvim',
		opts = {},
		dependencies = {
			'nvim-treesitter/nvim-treesitter',
			'nvim-tree/nvim-web-devicons'
		},
	},
	{
		'christoomey/vim-tmux-navigator',
		cmd = {
			'TmuxNavigateLeft',
			'TmuxNavigateDown',
			'TmuxNavigateUp',
			'TmuxNavigateRight',
			'TmuxNavigatePrevious',
		},
		keys = {
			{ '<c-h>', '<cmd><C-U>TmuxNavigateLeft<cr>' },
			{ '<c-j>', '<cmd><C-U>TmuxNavigateDown<cr>' },
			{ '<c-k>', '<cmd><C-U>TmuxNavigateUp<cr>' },
			{ '<c-l>', '<cmd><C-U>TmuxNavigateRight<cr>' },
			{ '<c-\\>', '<cmd><C-U>TmuxNavigatePrevious<cr>' },
		},
	},
	{
		'ThePrimeagen/refactoring.nvim',
		dependencies = {
			'nvim-lua/plenary.nvim',
			'nvim-treesitter/nvim-treesitter',
		},
		config = function()
			require('refactoring').setup()
		end,
	},
	{ 'ellisonleao/glow.nvim', config = true, cmd = 'Glow' }
}

require('lazy').setup(plugins, opts)

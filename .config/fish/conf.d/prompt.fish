# My fish prompt with shortened whoami, git status and return codes

function fish_prompt -d "Write out the prompt"

	set prompt

	set -a prompt " "
	set -a prompt (set_color -o purple) "("(cat /etc/hostname)")" (set_color -o normal)
	if fish_is_root_user	
		set -a prompt (set_color -o red) " (!)" (set_color normal)
	else
		set -a prompt \
			(set_color -io cyan) \
			" ("(string shorten --max 1 --char="" $USER)")" \
			(set_color normal)
	end

	set -a prompt \
		(set_color -o normal) \
		(string replace " " "" " "(fish_git_prompt)) \
		(set_color normal)
	
	set -a prompt (set_color -o normal) " ❯ " (set_color normal)
	printf "%s" $prompt
end

function fish_right_prompt -d "Write out the right prompt"

	set prompt
     	set -l _status $status

	if fish_is_root_user
		set -a prompt (set_color -o $fish_color_cwd_root)
	else
		set -a prompt (set_color -o $fish_color_cwd)	
	end

	set -a prompt (prompt_pwd) (set_color normal)

	if [ $_status -eq 0 ]
		set -a prompt (set_color -o magenta)
	else
		set -a prompt (set_color -o yellow)
	end

	set -a prompt \
		" [$_status]" \
		(set_color normal)

	printf "%s" $prompt
end

function fish_mode_prompt -d "Write out the mode status prompt"
  set prompt
  switch $fish_bind_mode
    case default
      set -a prompt (set_color -o red) '[n]'
    case insert
      set -a prompt (set_color -o green) '[i]'
    case replace_one
      set -a prompt (set_color -o green) '[r]'
    case visual
      set -a prompt (set_color -o brmagenta) '[v]'
    case '*'
      set -a prompt (set_color -o red) '[?]'
  end
  set -a prompt (set_color normal)

  printf "%s" $prompt
end

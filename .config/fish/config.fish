export EDITOR='nvim'

alias dl-librescore='npx dl-librescore@latest'
alias vi='nvim'

function start_vm_ps -d "Detach graphical session and start specifed VM"
	
	command sudo systemctl stop display-manager 
	command sudo systemctl stop sddm
	command sudo systemctl start libvirtd
	command sleep 5
	command sudo modprobe -r amdgpu
	command sudo modprobe vfio
	command sudo modprobe vfio_pci
	command sudo modprobe vfio_iommu_type1 
	command virsh --connect qemu:///system start $argv
end

if status is-interactive
	fish_vi_key_bindings
	set -g fish_greeting
	set -U dangerous_nogreeting
	bind -M insert \cf forward-word
	bind -M insert \ce accept-autosuggestion
end

for f in $HOME/.config/fish/conf.d/*.fish;
	source $f
end

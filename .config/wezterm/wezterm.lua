local wezterm = require 'wezterm'
local config = wezterm.config_builder()

-- My changes
config.color_scheme = 'Afterglow'
config.window_background_opacity = 0.9
config.enable_tab_bar = false

return config
